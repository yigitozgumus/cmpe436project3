//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment 3
/* This program shows Pnueli algorith for two processes*/
/*
loop forever do begin
l1: Noncritical section
l2: (yi,s) := (1,i);
l3: wait until (y1−i = 0) | (s != i);
l4: Critical section
l5: yi :=0; end

*/
byte y[2]; /*Local variables of processes*/
byte s;
byte mutexCheck;
byte pid1,pid2;

active [2] proctype P(){
  byte i = _pid;
  do
  :: 
    atomic {
      y[i] = 1;
      s = i;
      }
     (y[1-i] == 0) | (s != i);
     mutexCheck++;
  CS:   /*Critical Section*/
     mutexCheck--;
     y[i] = 0;
  od
}

/* For 2-a */
//ltl invariant { []((P@CS) -> (mutexCheck == 1)) }
/* For 2-b*/
//ltl unboundOvertake{ ([] ((s == _pid) & (y[1 - _pid] == 0)) -> <> (P[_pid]@CS))}
/* For 2-c */
//ltl infinity { ([]<> ((s == _pid) & (y[1 - _pid] == 0))) -> []<> (P[_pid]@CS)}

/* For 2-a */
never invariant{
  do
  ::
  assert (mutexCheck == 0 || mutexCheck == 1);
  od
}

/* For 2-b */
never unboundOvertake {    /* ([] ((s == _pid) & (y[1 - _pid] == 0)) -> <> (P[_pid]@CS)) */
T0_init:
  do
  :: atomic { (((! (((s == _pid) & (y[1 - _pid] == 0)))) || ((P[_pid]@CS)))) -> assert(!(((! (((s == _pid) & (y[1 - _pid] == 0)))) || ((P[_pid]@CS))))) }
  :: (1) -> goto T0_init
  od;
accept_all:
  skip
}

/* For 2-c */
never infinity {    /* ([]<> ((s == _pid) & (y[1 - _pid] == 0))) -> []<> (P[_pid]@CS) */
T0_init:
  do
  :: ((P[_pid]@CS)) -> goto accept_S10
  :: (1) -> goto T0_S10
  :: (! (((s == _pid) & (y[1 - _pid] == 0)))) -> goto accept_S26
  :: (1) -> goto T0_S23
  od;
accept_S10:
  do
  :: (1) -> goto T0_S10
  od;
accept_S26:
  do
  :: (! (((s == _pid) & (y[1 - _pid] == 0)))) -> goto accept_S26
  od;
T0_S10:
  do
  :: ((P[_pid]@CS)) -> goto accept_S10
  :: (1) -> goto T0_S10
  od;
T0_S23:
  do
  :: (! (((s == _pid) & (y[1 - _pid] == 0)))) -> goto accept_S26
  :: (1) -> goto T0_S23
  od;
}





