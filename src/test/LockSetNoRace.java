//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment 3

package test;
/**
 * Created by yigitozgumus on 11/7/16.
 */
public class LockSetNoRace {

   
    static int counter;

    static synchronized void increment(){
        counter++;
    }


    public static void main(String[] args) {

        final BinarySemaphore mutex1 =new BinarySemaphore(true);
        final BinarySemaphore mutex2 =new BinarySemaphore(true) ;
       
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                
                increment();
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                increment();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
