//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment 3
package test;
/**
 * Created by yigitozgumus on 11/7/16.
 */

public class LockSetRace {

   final BinarySemaphore mutex1 ;
   final BinarySemaphore mutex2 ;
    static int counter;


    public  LockSetRace(){
        mutex1 = new BinarySemaphore(true);
        mutex2 = new BinarySemaphore(true);
    }

    public static void main(String[] args) {
        LockSetRace test = new LockSetRace();
        test.execute();
    }

    public void execute(){
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                //============
                try {
                    mutex1.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    mutex2.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                increment();
                mutex2.V();
                mutex1.V();
                //============
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //============
                try {
                    mutex2.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                increment();
                mutex2.V();
                //============
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //============
                try {
                    mutex1.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                increment();
                mutex1.V();
                //============
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mutex1.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                increment();
                mutex1.V();
                //============
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //============

                try {
                    mutex2.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                increment();
                mutex2.V();
                //============
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //============
                try {
                    mutex1.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    mutex2.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                increment();

                mutex2.V();
                mutex1.V();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void increment(){
        counter++;
    }

}
