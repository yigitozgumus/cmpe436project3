//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment 3
package test;
/**
 * Created by yigitozgumus on 11/8/16.
 */
class BinarySemaphore {
    private boolean value;
    BinarySemaphore(boolean initValue){
        this.value = initValue;
    }
    synchronized void P() throws InterruptedException {
        while(!value)
            wait();
        value = false;
    }
    synchronized void V(){
        value = true;
        notify();
    }

}
