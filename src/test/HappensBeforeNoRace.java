//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment 3

package test;
/**
 * Created by yigitozgumus on 11/7/16.
 */
public class HappensBeforeNoRace {

    static int counter ;

    public static void main(String[] args) throws InterruptedException {

        final BinarySemaphore mutex1 = new BinarySemaphore(false);
        final BinarySemaphore mutex2 = new BinarySemaphore(false);
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mutex1.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counter++;
                mutex2.V();
                
            }

        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                mutex1.V();
                try {
                    mutex2.P();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counter++;
               
                
                

            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();

    }
}
